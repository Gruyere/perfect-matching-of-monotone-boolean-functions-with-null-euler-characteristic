#!/bin/bash

printf "========== ALL_INEQ\n"
k=1
while [ -d "generated/sperner/all_ineq/$k" ]; do
  printf "In generated/sperner/all_ineq/$k:" 
  l=1  
  while [ -f "generated/sperner/all_ineq/$k/$l" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

printf "\n========== ALL_INEQ_NONDEGENERATE\n"
k=1
while [ -d "generated/sperner/all_ineq_nondegenerate/$k" ]; do
  printf "In generated/sperner/all_ineq_nondegenerate/$k:" 
  l=1  
  while [ -f "generated/sperner/all_ineq_nondegenerate/$k/$l" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

printf "\n========== ALL_INEQ_NONDEGENERATE_ZERO_EULER\n"
k=1
while [ -d "generated/sperner/all_ineq_nondegenerate_zero_Euler/$k" ]; do
  printf "In generated/sperner/all_ineq_nondegenerate_zero_Euler/$k:" 
  l=1  
  while [ -f "generated/sperner/all_ineq_nondegenerate_zero_Euler/$k/$l" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

printf "\n========== ALL_INEQ_NONDEGENERATE_ZERO_EULER solutions\n"
k=1
while [ -d "generated/sperner/all_ineq_nondegenerate_zero_Euler/$k" ]; do
  printf "In generated/sperner/all_ineq_nondegenerate_zero_Euler/$k:" 
  l=1  
  while [ -f "generated/sperner/all_ineq_nondegenerate_zero_Euler/$k/$l""_solutions" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

