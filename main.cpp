#include <string>
#include <vector>
#include <map>
#include <unordered_set>
#include <boost/functional/hash.hpp> // https://www.quora.com/How-can-I-declare-an-unordered-set-of-pair-of-int-int-in-C++11

#include <sstream>
#include <fstream>
#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include <queue>

using namespace std;

typedef uint_fast16_t myint; //Use unsigned int for bit manipulations. Actually, I could even work with uint_fast8_t (since I won't use this code for more than 8 base elements), but apparently it is not advised, see https://www.learncpp.com/cpp-tutorial/fixed-width-integers-and-size-t/

namespace std { //this comes from here https://stackoverflow.com/questions/29855908/c-unordered-set-of-vectors. I do not understand it, but I replaced int by myint where appropriate. I hope this works.
  template <> struct hash<std::vector<myint>> {
    size_t operator()(const vector<myint>& v) const {
      std::hash<myint> hasher;
      size_t seed = 0;
      for (myint i : v) {
        seed ^= hasher(i) + 0x9e3779b9 + (seed<<6) + (seed>>2);
      }
      return seed;
    }
  };
}

/* INPUT: integer k.
   OUTPUT: the graph of the Hasse diagram of the powerset of {0,...,k+1}.
 */
map<myint,vector<myint>> powerset_graph(int k){
  map<myint,vector<myint>> graph;
  for (myint elem = 0; elem < static_cast<myint>(1 << (k+1)); ++elem){
    graph[elem] = vector<myint>(0);
  }
  deque<myint> queue;
  unordered_set<myint> seen;
  myint elem = myint(0);
  myint next_elem;
  queue.push_front(elem);
  seen.insert(elem);
  while(!queue.empty()){
    elem = queue.front(); queue.pop_front();
    for (int i = 0; i <= k; ++i){
      next_elem = elem | (1 << i);
      if (!seen.count(next_elem)){queue.push_back(next_elem); seen.insert(next_elem);}
      if (next_elem != elem){
        graph[elem].push_back(next_elem);
        graph[next_elem].push_back(elem);
      }
    }
  }
  return graph;
}


/* INPUT: a graph, a subset of edges
   OUTPUT: graphviz code for this graph with the specified edges colored
 */
string to_dot(int k, const map<myint,vector<myint>> &graph, const unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> &edges){
  string dotstring = "graph G{\nrankdir = BT;\n";
  for (pair<myint, vector<myint>> li: graph){
    string bitset_string;
    to_string(boost::dynamic_bitset<>(k+1, li.first), bitset_string);
    dotstring += "n_" + to_string(li.first) + " [label=\"" + bitset_string + "\"";
    dotstring += "];\n";
  }
  for (pair<myint, vector<myint>> li: graph){
    for (myint neigh: li.second){
      if (li.first < neigh){
        dotstring += "n_" + to_string(li.first) + " -- n_" + to_string(neigh) + " ";
        if (edges.count(pair<myint,myint>(li.first,neigh))) dotstring += "[color=\"deeppink2\", penwidth=2]"; 
        else dotstring+= "[penwidth=.5,style=dotted]";
        dotstring += ";\n";
      }
    } 
  }
  dotstring += "}";
  return dotstring;
}


/* INPUT: a filestream storing SPFs.
   OUTPUT: the next SPF stored in the file.
 */
vector<myint> get_next_SPF(ifstream &ifile){
  string line;
  getline(ifile,line);
  vector<myint> spf;
  istringstream iss(line);
  for(string s; iss >>s; ){
    spf.push_back(static_cast<myint>(stoi(s)));
  }
  return spf;
}


/* INPUT: a set (myint) and an SPF.
   OUTPUT: true if the set is a superset of one of the sets in the SPF, false otherwise.
 */
bool is_above(const myint elem, const vector<myint> &spf){
  for (myint set: spf){
    if ( (set & elem) == set ) return true;
  }
  return false;
}


/* INPUT: an SPF over {0,..,k}.
   OUTPUT: the set of subsets of {0,..,k} that are supersets of some set in the SPF.
 */
unordered_set<myint> above(int k, const vector<myint> &spf){
  unordered_set<myint> result;
  for (myint set = 0; set < static_cast<myint>(1 << (k+1)); ++set){
    if (is_above(set, spf)) result.insert(set);
  }
  return result;
}


/* INPUT: an SPF over {0,..,k}.
   OUTPUT: the set of subsets of {0,..,k} that are strict subsets of all the sets in the SPF.
 */
unordered_set<myint> strictly_below(int k, const vector<myint> &spf){
  unordered_set<myint> result;
  for (myint set = 0; set < static_cast<myint>(1 << (k+1)); ++set){
    if (!is_above(set, spf)) result.insert(set);
  }
  return result;
}


/* INPUT: a graph and a subset of the edges.
   OUTPUT: draws in tmp/mbf the graph, with the edges in the subset being colored.
 */
void draw(int k, const map<myint, vector<myint>> &graph, const unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> &edges){
  string dotstring = to_dot(k, graph, edges);
  ofstream dotfile("tmp/mbf.dot");
  dotfile << dotstring;
  dotfile.close();
  system("dot -Tpdf -o tmp/mbf.pdf tmp/mbf.dot");
}


/* INPUT: a graph and a subset of the nodes.
   OUTPUT: the induced subgraph.
 */
map<myint, vector<myint>> take_subgraph(const map<myint, vector<myint>> &graph, const unordered_set<myint> &nodes){
  map<myint, vector<myint>> subgraph;
  for (pair<myint, vector<myint>> li: graph){
    if (nodes.count(li.first)){
      subgraph[li.first] = vector<myint>(0);
      for (myint neigh: li.second){
        if (nodes.count(neigh)) subgraph[li.first].push_back(neigh);
      }
    } 
  }
  return subgraph;
}


// Number of lines in the file "generated/sperner/all_ineq_nondegenerate_zero_Euler/k/l".
int number_lines(int k, int l){
  int number_of_lines = 0;
  ifstream ifile ("generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l));
  string line;
  while (std::getline(ifile, line)) ++number_of_lines;
  return number_of_lines;
}


/* INPUT: a set, as a myint
   OUTPUT: true if its size is even, false otherwise.
*/
bool even(myint set){
  myint count = 0; 
  while (set) { 
      count += set & 1; 
      set >>= 1; 
  } 
  if (count%2 == 0) return true;
  return false;
}


// Helper function for the next function
vector<myint> reconstruct_path(const map<myint,myint> pred, myint current){
  vector<myint> path(0);
  path.push_back(current); 
  while (pred.count(current)){
    current = pred.at(current);
    path.push_back(current);
  }
  return path;
}


/* INPUT: a bipartite graph, a matching, and the set of vertices that are not touched by the matching
   OUTPUT: an augmenting path; see https://en.wikipedia.org/wiki/Matching_(graph_theory).
   Comment: in the bipartite graphs that we consider, two nodes (sets) are in the same partition iff their sizes have the same parity.
*/
pair<bool, vector<myint>> get_augmenting_path(const map<myint,vector<myint>> &graph, const unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> &matching, unordered_set<myint> &free){
  unordered_set<myint> seen;
  deque<myint> queue;
  map<myint,myint> pred;//to reconstruct the path if there is one
  for (auto &i: free){
    if (even(i)){//start with the free vertices of even hamming parity
      seen.insert(i);
      queue.push_back(i);
    }
  }
  myint current;
  while (!queue.empty()){
    current = queue.front(); queue.pop_front();
    if (!even(current) && free.count(current)){//We have our augmenting path
      vector<myint> path = reconstruct_path(pred, current);
      return pair<myint,vector<myint>>(true, path);
    }
    if (even(current)){//explore all edges that are not in the matching
      for (auto &neigh: graph.at(current)){
        pair<myint,myint> edge((current < neigh)? current: neigh, (current < neigh)? neigh: current); 
        if (!matching.count(edge) && !seen.count(neigh)){ queue.push_back(neigh); seen.insert(neigh); pred[neigh] = current;}
      }
    }
    if (!even(current)){//find the edge containing current that is in the matching (we know there is one because current is not free)
      for (auto &neigh: graph.at(current)){
        pair<myint,myint> edge((current < neigh)? current: neigh, (current < neigh)? neigh: current); 
        if (matching.count(edge) && !seen.count(neigh)){ queue.push_back(neigh); seen.insert(neigh); pred[neigh] = current;}
      }
    }
  }

  return pair<bool, vector<myint>>(false, vector<myint>(0));
}


/* INPUT: a bipartite graph.
   OUTPUT: a maximum cardinality matching; see https://en.wikipedia.org/wiki/Maximum_cardinality_matching
 */
unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> mcm(map<myint, vector<myint>> &graph){
  unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> matching;
  unordered_set<myint> free; // the vertices that are currently not matched.
  for (auto &li: graph){//initially all vertices are free
    free.insert(li.first);
  }
  pair<bool, vector<myint>> am = get_augmenting_path(graph, matching, free);
  while (am.first){
    // augment the matching, remove first and last element in the path from free
    free.erase(am.second[0]);
    for (int i=0; i < static_cast<int>(am.second.size()) - 1; ++i){
      pair<myint, myint> edge((am.second[i] < am.second[i+1]) ? am.second[i] : am.second[i+1], (am.second[i] < am.second[i+1]) ? am.second[i+1] : am.second[i]);
      if (i%2 == 0) matching.insert(edge); 
      else matching.erase(edge);
    }
    free.erase(am.second[am.second.size()-1]);
    am = get_augmenting_path(graph, matching, free);
  }

  return matching;
}


/* INPUT: a set of sets.
   OUTPUT: the Euler characteristic of that set.
 */
int eul_char(const unordered_set<myint> &set){
  int eul = 0;
  for (auto &s: set) eul += (even(s))? 1 : -1;
  return eul;
}

/* INPUT: a set of spfs.
   OUTPUT: write them in the file.
 */
void write_file(const unordered_set<vector<myint>> & spfs, const string filepath){
  string thestring = "";
  for(auto &spf: spfs){
    for(size_t i = 0; i < spf.size(); ++i){
      thestring += to_string(spf[i]) + " ";
    }
    thestring.pop_back();//remove trailing space
    thestring += "\n";
  }
  thestring.pop_back();//remove trailing \n
  ofstream thefile(filepath); 
  thefile << thestring;
  thefile.close();
}

/* INPUT: the path of a file containing an SPF on each line. An SPF is simply specified by a sorted list of sets, where each set is represented by an int (which we see as a bitset).
 */
vector<vector<myint>> read_file(const string filepath){
  ifstream myfile (filepath); 
  vector<vector<myint>> result;
  if (myfile.is_open()){
    string line;
    while ( getline(myfile,line) ){
      vector<myint> spf;
      istringstream iss(line);
      for(string s; iss >>s; ){
        spf.push_back(static_cast<myint>(stoi(s)));
      }
      sort(spf.begin(),spf.end());// It should already be sorted but who knows
      result.push_back(spf);
    }
  }
  cout << "Number of SPFs loaded: " << result.size() << "\n";
  return result;
}

/* INPUT: k, l 
   OUTPUT: reads all SPFs stored in file generated/sperner/all_ineq_nondegenerate/k/l, and
           puts those having zero Euler characteristic into generated/sperner/all_ineq_nondegenerate_zero_Eul/k/l
 */
void filter_nonzero_euler(const int k, const int l){
  cout << "========================\n";
  cout << "You want to generate all inequivalent Sperner families of level " << l << " that are nondegenerate and have a zero Euler characteristic, for a base set of " << k+1 << " elements\n";
  cout << "========================\n";
  ifstream ifile;
  
  //Some safeguards.
  if (l < 1){
    cout << "The level must be > 0.\n\n";
    return;
  }
  ifile.open("generated/sperner/all_ineq_nondegenerate_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l));
  if (ifile){
    cout << "It seems that you have already generated this. If you really want to recompute it, please delete the file \"generated/sperner/all_ineq_nondegenerate_zero_Euler/" << k << "/" << l << "\"\n\n";
    return;
  }
  ifile.close();
  ifile.open("generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l));
  if (!ifile){
    cout << "You need to have generated all the inequivalent nondegenerate SPFs of level " << l << " before\n\n";
    return;
  }
  ifile.close();

  //Some info.
  cout << "Loading nondegenerate level " << l << "...\n";
  vector<vector<myint>> level = read_file("generated/sperner/all_ineq_nondegenerate/" + to_string(k) + "/" + to_string(l));

  //Do it.
  cout << "Now keeping only those that have zero Euler characteristic...\n";
  unordered_set<vector<myint>> result;
  for (size_t i = 0; i < level.size(); ++i){
    if (!eul_char(above(k,level[i]))) result.insert(level[i]);
  }
  cout << "There are " + to_string(result.size()) + " such SPFs\n";
  string filepath = "generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l);
  cout << "Writing this into " + filepath << " ...\n";
  write_file(result, filepath);
  cout << "Done.\n\n";
  }


/* INPUT: an SPF whose corresponding MBF has zero Euler characteristic
   OUTPUT: the first number is the number of nodes that are not matched in an mcm for the upgraph,
           the second number is the same but for the downgraph
 */
pair<int,int> test_one_MBF(int k, const map<myint,vector<myint>> &powersetgraph, const vector<myint> &spf){
  unordered_set<myint> sat = above(k, spf);
  //Test the upgraph
  map<myint,vector<myint>> upgraph = take_subgraph(powersetgraph, sat);
  unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> max_match = mcm(upgraph);
  int first = sat.size() - 2 * max_match.size();
  //Test the downgraph
  unordered_set<myint> unsat = strictly_below(k, spf);
  map<myint,vector<myint>> downgraph = take_subgraph(powersetgraph, unsat);
  max_match = mcm(downgraph);
  int second = unsat.size() - 2 * max_match.size();
  return pair<int,int>(first, second);
}


/* INPUT: 
   OUTPUT: test the conjecture on all the SPFs stored in file
   "generated/sperner/all_ineq_nondegenerate_zero_Euler/k/l", starting from line
    start_line. Puts line numbers and the number of untouched nodes in
    generated/sperner/all_ineq_nondegenerate_zero_Euler/k/l_solutions the upgraph
    and the downgraph in .  Advertise on stdout when it finds one for which the
    conjecture is false.
 */
void play_on_all_MBFs(int k, int l){
  cout << "=================\n You want to test the conjecture on all the MBFs generated by the SPFs stored in file \"generated/sperner/all_ineq_nondegenerate_zero_Euler/" <<k << "/" << l <<"\"\n==================\n";
  //Some safeguards.
  ifstream ifile;
  ifile.open("generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l) + "_solutions");
  if (ifile){
    cout << "File \"generated/sperner/all_ineq_nondegenerate_zero_Euler/" << k << "/" << l << "_solutions already exists\". Delete it if you want to recompute it.\n\n";
    return;
  }
  ifile.close();
  ifile.open("generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l));
  if (!ifile){
    cout << "The file generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l) << " does not exist.\n\n";
    return;
  }
  int nb_lines=number_lines(k,l);
  cout << "There are " << nb_lines << " SPFs in this file.\n";
  map<myint, vector<myint>> powersetgraph = powerset_graph(k);
  string thestring = "";
  string line;
  vector<myint> spf;
  //SPF line number: number_of_untouched_nodes_in_upgraph \t number_of_untouched_nodes_in_downgraph
  for (int j = 1; j <= nb_lines; ++j){
    spf = get_next_SPF(ifile);
    pair<int,int> result = test_one_MBF(k, powersetgraph, spf);
    thestring += to_string(j) + ": " + to_string(result.first) + "\t" + to_string(result.second) + "\n";
    if (result.first & result.second) cout << "\nThe conjecture is false for the MBF on line " << j << "\n";
  }
  thestring.pop_back();//remove trailing \n
  ofstream thefile("generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l) + "_solutions"); 
  thefile << thestring;
  thefile.close();
  return;
}


/* INPUT: 
   OUTPUT: test the conjecture on the MBF generated by the SPF stored in line j of file generated/sperner/all_ineq_nondegenerate_zero_Euler/k/l.
           draws the MBF as well as the perfect matchings if they exist
 */
void test_and_draw_one_MBF(int k, int l, int j){
  //Some safeguards.
  ifstream ifile ("generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l));
  if (!ifile){
    cout << "The file generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l) << " does not exist.\n\n";
    return;
  }
  if (j < 1){cout << "Line number must be > 0\n"; return;}
  int nb_lines=number_lines(k,l);
  if (j > nb_lines ){
    cout << "The file generated/sperner/all_ineq_nondegenerate_zero_Euler/" + to_string(k) + "/" + to_string(l) << " has only " << nb_lines << " lines.\n\n";
    return;
  }
  
  //Fetch the SPF and show it to user.
  string line;
  for (int r=1; r<j; ++r) getline(ifile,line);
  vector<myint> spf = get_next_SPF(ifile);
  ifile.close();
  cout << "=======> You want to test the conjecture on the MBF generated by the SPF spf = ";
  for (auto set: spf) cout << set << " ";
  cout << "(as bitsets: ";
  vector<boost::dynamic_bitset<>> spf_as_bitsets(0);
  for (auto set:spf) spf_as_bitsets.push_back(boost::dynamic_bitset<>(k+1,set));
  for (auto set: spf_as_bitsets) cout << set << " ";
  cout << ")\n";

  string thestring = to_string(k+1) + "\n";
  unordered_set<myint> sat = above(k, spf);
  for (auto set: sat) thestring += to_string(set) + " ";
  thestring.pop_back();//remove trailing space
  thestring += "\n";
  //Test the upgraph
  map<myint, vector<myint>> powersetgraph = powerset_graph(k);
  map<myint,vector<myint>> upgraph = take_subgraph(powersetgraph, sat);
  unordered_set<pair<myint, myint>, boost::hash <pair <myint, myint>>> max_match = mcm(upgraph);
  if (sat.size() == 2 * max_match.size()){
    cout << "The upgraph has a perfect matching.\n";
    for (auto pair: max_match) thestring += to_string(pair.first) + " " + to_string(pair.second) + ";";
    thestring.pop_back(); thestring += "\n";
  }
  else{
    thestring += "0 0\n";
  }

  //Now try the downgraph
  unordered_set<myint> unsat = strictly_below(k, spf);
  map<myint,vector<myint>> downgraph = take_subgraph(powersetgraph, unsat);
  max_match = mcm(downgraph);
  if (unsat.size() == 2 * max_match.size()){
    cout << "The downgraph has a perfect matching.\n";
    for (auto pair: max_match) thestring += to_string(pair.first) + " " + to_string(pair.second) + ";";
    thestring.pop_back(); thestring += "\n";
  }
  else{
    thestring += "0 0\n";
  }
  ofstream thefile(to_string(k) + "-" + to_string(l) + "-" + to_string(j) + ".tmp"); 
  thefile << thestring;
  thefile.close();
  //https://stackoverflow.com/questions/21589353/cannot-convert-stdbasic-stringchar-to-const-char-for-argument-1-to-i
  system(("python draw.py " + to_string(k) + "-" + to_string(l) + "-" + to_string(j) + ".tmp " + "1").c_str());
}


int main(int arc, char** argv){

  #ifdef FILTER
  filter_nonzero_euler(stoi(argv[1]), stoi(argv[2]));
  #endif

  #ifdef ONE
  test_and_draw_one_MBF(stoi(argv[1]), stoi(argv[2]), stoi(argv[3]));
  #endif

  #ifdef ALL
  play_on_all_MBFs(stoi(argv[1]), stoi(argv[2]));
  #endif
}
