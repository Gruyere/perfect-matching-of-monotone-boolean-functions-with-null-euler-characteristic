#!/usr/bin/env python3

import os, sys
import itertools


'''
  given a sorted tuple of elements, concatenate them all to give a name to the tikz node.
'''
def name(tup):
  if len(tup)==0: return 'emptyset'
  return ''.join((str(el) for el in tup))

'''
  given a sorted tuple of elements, concatenate them all to give a label to the tikz node.
'''
def label(tup):
  if len(tup)==0: return '$\\emptyset$'
  return '$' + ''.join((str(el) for el in tup)) + '$' # shortlabel
#  return '$\\{' + ''.join((str(el) + ', ' for el in tup))[:-2] + '\\}$' # longlabel

'''
  given a sorted tuple and position and Boolean color, generates tikz code that draws the node. If color is True then apply a color.
'''
def draw_node(tup, x_pos, y_pos, color):
  if color: return '\\node[nodestyle,fill=amber] (' + name(tup) + ') at (' + x_pos + ', ' + y_pos + ') {' + label(tup) + '};\n'
  return '\\node[nodestyle] (' + name(tup) + ') at (' + x_pos + ', ' + y_pos + ') {' + label(tup) + '};\n'

'''
  given a sorted tuple, returns the integer that represent the same set.
'''
def set_as_int(tup):
  ret = 0
  for elem in tup:
    ret += 1<<elem
  return ret

def string_to_int(st):
  ret = 0
  for elem in st:
    ret += 1 << int(elem)
  return ret

def readfile1(filepath):
  with open(filepath, "r") as file:
    n = int(file.readline())
    coloredNodes = [int(s.strip()) for s in file.readline().split()]
    coloredEdges1 = [tuple(sorted((int(s.split()[0].strip()),int(s.split()[1].strip())))) for s in file.readline().split(";")]
    coloredEdges2 = [tuple(sorted((int(s.split()[0].strip()),int(s.split()[1].strip())))) for s in file.readline().split(";")]
  return (n,coloredNodes,coloredEdges1,coloredEdges2)


def readfile2(filepath):
  with open(filepath, "r") as file:
    n = int(file.readline())
    coloredNodes = [string_to_int(s.strip()) for s in file.readline().split()]
    coloredEdges = [tuple(sorted((string_to_int(s.split()[0].strip()),string_to_int(s.split()[1].strip())))) for s in file.readline().split(";")]
  return (n,coloredNodes,coloredEdges)

if __name__ == '__main__':
  os.system('rm tmp/*')
  if sys.argv[2] == '2': (n,coloredNodes,coloredEdges1,coloredEdges2) = readfile2(sys.argv[1])
  if sys.argv[2] == '1': (n,coloredNodes,coloredEdges1,coloredEdges2) = readfile1(sys.argv[1])

  
  H_SPACING = [1, 1, 1, 1, 1, 1.2, 1.4, 1.4, 2.0]
  V_SPACING = [1, 1, 1, 1, 1, 1.4, 2.8, 2.8, 2.5]
  elements = range(n)

  tikzcode = '\\documentclass{standalone}\n\\usepackage{tikz}\n\\begin{document}\n\\definecolor{amaranth}{rgb}{0.9, 0.17, 0.31}\n\\definecolor{amber}{rgb}{1.0, 0.49, 0.0}\n\\definecolor{dartmouthgreen}{rgb}{0.05, 0.5, 0.06}\n\n\\begin{tikzpicture}\n\\tikzset{nodestyle/.style={draw,rectangle}}\n\n'
  #===================== DRAW THE NODES ========================
  tikzcode += ' ===== NODES ====\n\n'

  for i in range(n+1):
    subsets_size_i = list(itertools.combinations(elements,i)) # lexicographically sorted list of sorted tuples of i elements
    y_pos = str(float(i * V_SPACING[n]))
    h_length = H_SPACING[n] * (len(subsets_size_i)-1) # total length that the i-th layer will occupy
    for j in range(len(subsets_size_i)):
      x_pos = str(float(-h_length/2 + j * H_SPACING[n]))
      tikzcode += draw_node(subsets_size_i[j], x_pos, y_pos, set_as_int(subsets_size_i[j]) in coloredNodes)

  # ===================== DRAW THE EDGES ========================
  tikzcode += '\n\n ===== EDGES ====\n\n'

  for i in range(n):
    subsets_size_i = list(itertools.combinations(elements,i))
    for subset_size_i in subsets_size_i:
      for r in [el for el in range(n) if el not in subset_size_i]:
        neigh = tuple(sorted(subset_size_i + (r,))) # because sorted(tuple) returns a list 
        if (tuple(sorted((set_as_int(subset_size_i),set_as_int(neigh)))) in coloredEdges1):
          tikzcode+= '\\draw[amaranth,thick] '
          tikzcode += '(' + name(subset_size_i) + ') -- (' + name(neigh) + ');\n'
        elif (tuple(sorted((set_as_int(subset_size_i),set_as_int(neigh)))) in coloredEdges2):
          tikzcode+= '\\draw[dartmouthgreen,thick] '
          tikzcode += '(' + name(subset_size_i) + ') -- (' + name(neigh) + ');\n'
        else:
          tikzcode += '\\draw[black,dotted] '
          tikzcode += '(' + name(subset_size_i) + ') -- (' + name(neigh) + ');\n'
  tikzcode += '\n\\end{tikzpicture}\n\\end{document}'

  f = open("tmp/" + sys.argv[1] +".tex", 'w')
  f.write(tikzcode)
  f.close()
  os.system('cd tmp/; latexmk -pdf ' + sys.argv[1] + '.tex')
