# About

This is a better version of the code that I used in the AMW''18 workshop paper
“[Towards Deterministic Decomposable Circuits for Safe
Queries](https://arxiv.org/abs/1912.11098)”.  I realized that the conjecture
presented in this paper can be equivalently stated as the problem of
determining if a perfect matching in some particular graphs always exist. This
new formulation is described
[here](https://cstheory.stackexchange.com/q/42626/38111), and the explanation
of why they are the same problem can be found in the PODS'20 paper “[Solving a
Special Case of the Intensional vs Extensional Conjecture in Probabilistic
Databases](https://arxiv.org/abs/1912.11864)” (the new formulation appears as
*CONJECTURE 1* on page 10). The ameliorations compared to the original code are
the following:

  - Instead of using a SAT solver, I now use a simple algorithm to find a
maximum cardinality matching in bipartite graphs.
  - To generate the monotone Boolean functions, I now use
[https://gitlab.com/Gruyere/sperner-families-generator](https://gitlab.com/Gruyere/sperner-families-generator).
This is the C++ version of what I used in the original Python code (hence, much
faster).
  - Generally speaking, the code is cleaner and better documented.

The original code can still be found at
[http://mikael-monet.net/code/q9_code.tar.gz](http://mikael-monet.net/code/q9_code.tar.gz),
but really you shouldn't use it.

# Requirement

1. A C++ compiler (like g++)
2. The C++ code uses boost's library dynamic_bitsets. You can install boost with `sudo apt-get install libboost-all-dev`
3. To visualize the perfect matchings, you will need the program “dot” from graphviz (to draw graphs): `sudo apt install graphviz`

# How to use

### Generating monotone Boolean functions

First, you want to generate some monotone Boolean functions (MBF). Note that
this is the same as generating monotone
[DNFs](https://en.wikipedia.org/wiki/Disjunctive_normal_form), hence the same
as generating [Sperner families](https://en.wikipedia.org/wiki/Sperner_family).
I have [another project](https://gitlab.com/Gruyere/sperner-families-generator)
for that. You need to clone it in a directory “generated” under the main
directory:

  - `git clone git@gitlab.com:Gruyere/sperner-families-generator.git generated`

Read the README and generate what we call there “nondegenerate inequivalent Sperner families”.
Then run:

  -  `mkdir generated/sperner/all_ineq_nondegenerate_zero_Euler` and 
  -  `for i in {1..8}; do mkdir generated/sperner/all_ineq_nondegenerate_zero_Euler/$i ; done`
  -  `g++ -Wall -DFILTER -O3 main.cpp`

Now you can use `./a.out k l` to read all MBFs stored in
`generated/sperner/all_ineq_nondegenerate/k/l` and keep only those having a
zero Euler characteristic into
`generated/sperner/all_ineq_nondegenerate_zero_Euler/k/l`

### Testing the conjecture

1. Compile with `g++ -Wall -O3 main.cpp`
2. Use `./a.out k l j` to test the conjecture on the MBF stored on line $`j`$
of the file generated/sperner/all_ineq_nondegenerate/k/l. If the MBF has
nonzero Euler characteristic it will tell you so and stop (since there cannot
be any perfect matching in this case). If the Euler characteristic is zero, it
will first determine if a perfect matching of the graph “above” exists. If this
is the case the perfect matching is drawn in tmp/mbf.pdf. If this is not the
case it will determine if a perfect matching of the graph “below” exists, and
draw it if so. Else, the conjecture is false.

For instance, if you have generated the nondegenerate inequivalent SPFs for
$`k=4,l=5`$, then `./a.out 4 5 38` should produce the following drawing:

![](examples/k4l5j38.png)

with the perfect matching being the colored edges. More examples can be found in the directory examples/.

You can also test the conjecture on all the MBFs stored from line $`j`$ in file generated/sperner/all_ineq_nondegenerate/k/l by commenting and uncommenting the right lines in the main() of main.cpp, compiling, and `./a.out k l j`. It will not do the drawing, and it will stop as soon as it finds a counterexample (but so far none found).


